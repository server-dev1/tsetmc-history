<?php

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\FirePHPHandler;
use Carbon\Carbon;
use Morilog\Jalali\Jalalian;

function setupLogger($log_file_path = "debug.log", $logger_name = "logger") {
    $logger = new Logger($logger_name);
    $logger->pushHandler(new StreamHandler(__DIR__."/$log_file_path", Logger::DEBUG));
    $logger->pushHandler(new FirePHPHandler());
    return $logger;
}

function jsonRes(array $data, $status_code = 200)
{
    header("content-type: application/json; charset=utf-8");
    http_response_code($status_code);
    die(json_encode($data));
}

function createUrl($symbol_id) {
    return "http://api4.tablokhani.com/Process/singleFixedRows/$symbol_id?v=XUqm1";
}

function getSymbol($id) {
    $symbols = json_decode(file_get_contents(__DIR__ . "/data/symbols_name.json"), true);

    if (isset($symbols[$id])) {
        return $symbols[$id];
    }

    return $id;
}

function sendRequest($url, callable $errorHandler) {
    $response = file_get_contents($url);
    if (empty($response)) {
        return $errorHandler();
    }
    return $response;
}

function customFormat($data, $delimiter = "-")
{
    $year = substr($data, 0, 4);
    $month = substr($data, 4, 2);
    $day = substr($data, 6, 2);

    return "$year$delimiter$month$delimiter$day";
}

function getJDate($date_string, $format = "Y-m-d", $delimiter = "-")
{
    $jDate = Jalalian::fromCarbon(Carbon::createFromFormat($format, $date_string));
    return "{$jDate->getYear()}$delimiter{$jDate->getMonth()}$delimiter{$jDate->getDay()}";
}

function setDebugMode($mode = true) {
    if (!$mode) error_reporting(E_ERROR | E_PARSE);
}
