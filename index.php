<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>تاریخچه سهم</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="assets/icon/uicons/css/uicons-regular-rounded.css">
    <link rel="stylesheet" href="assets/icon/uicons/css/uicons-solid-rounded.css">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>
<body>
<header class="header">
    <div class="title-header">
        <a href="#">Tsetmc.com<span class="span-title-header">Manager</span></a>
    </div>
    <i class="fi fi-rr-search" style="vertical-align: middle; margin-left: 10px"></i>
    <input type="text" class="field field-symbol-name" id="search-field" placeholder="نام یه نماد سهم">
    <input type="text" class="field field-days" id="limit-field" placeholder="تعداد روزها" value="15">
    <button class="btn-search btn-with-ripple" id="bnt-search" style="width: 7%;">جستوجو</button>
    <div class="filter-box inline-child">
        <i class="fi fi-rr-filter" style="color: #333; vertical-align: middle"></i>
        <p class="filter-title">فیلتر</p>

        <div class="checkbox-content">
            <input type="checkbox" class="checkbox" data-column="row-number" id="col-row-number" checked>
            <label for="col-row-number" class="label-checkbox">ردیف</label>
        </div>

        <div class="checkbox-content">
            <input type="checkbox" class="checkbox" data-column="date" id="col-row-date" checked>
            <label for="col-row-date" class="label-checkbox">تاریخ</label>
        </div>
        <div class="checkbox-content">
            <input type="checkbox" class="checkbox" data-column="p-last" id="col-p-last" checked>
            <label for="col-p-last" class="label-checkbox">قیمت معامله</label>
        </div>
        <div class="checkbox-content">
            <input type="checkbox" class="checkbox" data-column="p-p-last" id="col-p-p-last" checked>
            <label for="col-p-p-last" class="label-checkbox">درصد معامله</label>
        </div>
        <div class="checkbox-content">
            <input type="checkbox" class="checkbox" data-column="p-close" id="col-p-close" checked>
            <label for="col-p-close" class="label-checkbox">قیمت پایانی</label>
        </div>
        <div class="checkbox-content">
            <input type="checkbox" class="checkbox" data-column="p-p-close" id="col-p-p-close" checked>
            <label for="col-p-p-close" class="label-checkbox">درصد پایانی</label>
        </div>
        <div class="checkbox-content">
            <input type="checkbox" class="checkbox" data-column="volume" id="col-volume-trad" checked>
            <label for="col-volume-trad" class="label-checkbox">حجم معاملات</label>
        </div>
        <div class="checkbox-content">
            <input type="checkbox" class="checkbox" data-column="value" id="col-value-trad" checked>
            <label for="col-value-trad" class="label-checkbox">ارزش معاملات</label>
        </div>
        <div class="checkbox-content">
            <input type="checkbox" class="checkbox" data-column="sarane-kh" id="col-sarane-kh" checked>
            <label for="col-sarane-kh" class="label-checkbox">سرانه خرید</label>
        </div>
        <div class="checkbox-content">
            <input type="checkbox" class="checkbox" data-column="sarane-f" id="col-sarane-f" checked>
            <label for="col-sarane-f" class="label-checkbox">سرانه فروش</label>
        </div>
        <div class="checkbox-content">
            <input type="checkbox" class="checkbox" data-column="ghodrat" id="col-ghodrat" checked>
            <label for="col-ghodrat" class="label-checkbox">قدرت</label>
        </div>
    </div>
</header>
<section class="loading-section d-none" id="loading-section">
    <img src="assets/image/loading.gif" alt/>
</section>
<section class="content d-none" id="content-history">
    <header class="header-content inline-child">
        <div class="title-content inline-child">
            <i class="fi fi-rr-star dark" style="font-size: 20px;cursor: pointer;"></i>
            <h1 id="symbolName"></h1>
        </div>
        <div class="btn-box">
            <div class="btn-with-ripple btn-tsetmc" id="btn_tsetmc">tsetmc</div>
        </div>
    </header>
    <p class="note-content">مقادیر برحسب میلیون تومان میباشد.</p>
    <!--------------------------chart-------------------------------------------->
    <div class="chart-content inline-child">
        <div class="card" id="chart-box-a" style=" height: 300px; width: 45%; display: inline-block">
            <canvas id="chartA"></canvas>
        </div>
        <div class="card" id="chart-box-b" style=" height: 300px; width: 45%; display: inline-block">
            <canvas id="chartB"></canvas>
        </div>
    </div>
    <!-------------------------chart--------------------------------------------->
    <table class="table">
        <thead>
        <tr>
            <th class="table-row table-header">سرانه خرید وزنی</th>
            <th class="table-row table-header">سرانه فروش وزنی</th>
            <th class="table-row table-header">قدرت وزنی</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="table-row" id="avgKharidVazni"></td>
            <td class="table-row" id="avgForoshVazni"></td>
            <td class="table-row" id="ghodratVazni"></td>
        </tr>
        </tbody>
    </table>
    <table class="table">
        <thead>
        <tr>
            <th class="table-row table-header col-row-number">#</th>
            <th class="table-row table-header col-date">تاریخ</th>
            <th class="table-row table-header col-p-last">قیمت معامله</th>
            <th class="table-row table-header col-p-p-last">درصد معامله</th>
            <th class="table-row table-header col-p-close">قیمت پایانی</th>
            <th class="table-row table-header col-p-p-close">درصد پایانی</th>
            <th class="table-row table-header col-volume">حجم معاملات</th>
            <th class="table-row table-header col-value">ارزش معاملات</th>
            <th class="table-row table-header col-sarane-kh">سرانه خرید</th>
            <th class="table-row table-header col-sarane-f">سرانه فروش</th>
            <th class="table-row table-header col-ghodrat">قدرت</th>
        </tr>
        </thead>
        <tbody id="tbody2">

        </tbody>
    </table>

</section>
<script src="assets/jquery/jquery.min.js"></script>
<script src="assets/js/chart.js"></script>
<script src="assets/js/chartView.js"></script>
<script src="assets/js/handelView.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script>
    toastr.options.closeButton = true;
    toastr.options.showMethod = 'slideDown';
    toastr.options.hideMethod = 'fadeOut';
    toastr.options.closeMethod = 'fadeOut';
    toastr.options.timeOut = 5000; // How long the toast will display without user interaction
    toastr.options.extendedTimeOut = 5000; // How long the toast will display after a user hovers over it
    toastr.options.progressBar = true;
    toastr.options.rtl = true;
    toastr.options.positionClass = "toast-bottom-left";
</script>
<script>

    let starAttr = $(".title-content").children("i")[0];
    let star_outline = "fi fi-rr-star dark";
    let star_fill = "fi-sr-star yellow fi";
    $(starAttr).click(function () {
        if (starAttr.className.includes("yellow")) {
            $(starAttr).removeClass(star_fill);
            $(starAttr).addClass(star_outline);
        } else {
            $(starAttr).removeClass(star_outline);
            $(starAttr).addClass(star_fill);
        }
    });


</script>
</body>
</html>