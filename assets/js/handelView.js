let tsetmcLink = "http://www.tsetmc.com/Loader.aspx?ParTree=151311&i=";
var symbolTsetmcLink = "";
let handleClick = function () {
    let symbol = $("#search-field").val().trim();
    let limit = $("#limit-field").val().trim();
    toastr.clear();
    if (symbol === "") {
        toastr.error("لطفا فیلد را پر کنید");
        return
    }

    let data = {
        symbol: symbol
    };
    if (limit !== "") {
        data.limit = limit
    }

    $("#content-history").addClass("d-none");
    $.ajax({
        data: data,
        type: "get",
        url: "processor.php",
        success: function (res) {
            $("#content-history").removeClass("d-none");
            clearData();
            renderData(res.history);
            setFilterToTable();
            console.log(res);
            $("#symbolName").text(res.symbolName);
            $("#avgKharidVazni").text(res.AvgKharidVazni);
            $("#avgForoshVazni").text(res.AvgForoshVazni);
            $("#ghodratVazni").text(res.GhodratVazni);

            symbolTsetmcLink = tsetmcLink + res.symbolId;

            $('#content-history').removeClass('d-none');
            $('#loading-section').addClass('d-none');

            showChart(res.history);

        },
        error: function (xhr) {
            console.log(xhr);
            toastr.error(JSON.parse(xhr.responseText).error);
            $('#loading-section').addClass('d-none');
            $('#content-history').addClass('d-none');
        }
    });

    $('#loading-section').removeClass('d-none');
};

$("#bnt-search").click(function () {
    handleClick()
});
$("#search-field").on('keypress', function (e) {
    if (e.which === 13) {
        handleClick()
    }
});
$("#limit-field").on('keypress', function (e) {
    if (e.which === 13) {
        handleClick()
    }
});
$("#btn_tsetmc").click(function () {
    window.open(symbolTsetmcLink);
});


function renderData(data) {
    data.forEach(item => {
        addRow(item, data.indexOf(item))
    })
}

function addRow(rowData, index) {
    $("#tbody2").append(`<tr>
            <td class="table-row row-row-number">${index + 1}</td>
            <td class="table-row row-date">${rowData.jDate}</td>
            <td class="table-row row-p-last">${rowData.last}</td>
            <td class="table-row row-p-p-last" style="direction: ltr;color: ${getNumColor(rowData.lastPercent)}">${rowData.lastPercent}</td>
            <td class="table-row row-p-close">${rowData.close}</td>
            <td class="table-row row-p-p-close" style="direction: ltr;color: ${getNumColor(rowData.closePercent)}">${rowData.closePercent}</td>
            <td class="table-row row-volume">${rowData.tradeVolume_view}</td>
            <td class="table-row row-value">${rowData.tradeValue_view}</td>
            <td class="table-row row-sarane-kh">${rowData.sarane_kh_view}</td>
            <td class="table-row row-sarane-f">${rowData.sarane_f_view}</td>
            <td class="table-row row-ghodrat">${rowData.buyer_ratio}</td>
        </tr>`);

    function getNumColor(num) {
        return num > 0 ? "green" : num < 0 ? "red" : "gray";
    }
}

function clearData() {
    $("#chart-box-a").text("");
    $("#chart-box-b").text("");
    $("#tbody").text("");
    $("#tbody2").text("");
}


let checkbox = $('input[type=checkbox][data-column]');
addRemoveCol(checkbox);


checkbox.change(function () {
    addRemoveCol($(this));
});

function addRemoveCol(checkbox) {
    let column = checkbox.attr('data-column');
    if (checkbox.prop('checked')) {
        $(`.col-${column}`).removeClass('d-none');
        $(`.row-${column}`).removeClass('d-none');
    } else {
        $(`.col-${column}`).addClass('d-none');
        $(`.row-${column}`).addClass('d-none');
    }
}

function setFilterToTable() {
    $('.filter-box input[type=checkbox]').toArray().forEach(element => {
        addRemoveCol($(element))
    })
}

function showChart(history) {
    let saraneKhAll = [];
    let saraneFAll = [];
    let buyerRatio = [];
    let volume = [];
    let days = [];

    $("#chart-box-a").append(`<canvas id="chartA"></canvas>`)
    $("#chart-box-b").append(`<canvas id="chartB"></canvas>`);



    history.forEach(item => {
        days.push(item.jDate);
        volume.push(item.tradeVolume);
        buyerRatio.push(item.buyer_ratio);
        saraneKhAll.push(parseFloat(item.sarane_kh_view));
        saraneFAll.push(parseFloat(item.sarane_f_view));
    });

    days.reverse();
    let dataYa = [saraneKhAll.reverse(), saraneFAll.reverse()];
    let dataYb = [buyerRatio.reverse(), volume.reverse()];
    let saraneChart = new SaraneChart(days, dataYa);
    let ghodratVolumeChart = new GhodratVolumeChart(days, dataYb);
}
