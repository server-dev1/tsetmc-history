function GhodratVolumeChart(dataX, dataY) {
    return new Chart($("#chartB"), {
        type: 'bar',
        data: {
            labels: dataX,
            datasets: [
                {
                    label: 'حجم معاملات',
                    type: 'bar',
                    yAxisID: 'A',
                    backgroundColor: 'rgba(102,187,106,0.58)',
                    data: dataY[1]
                },
                {
                    label: 'قدرت خریدار',
                    type: 'line',
                    backgroundColor: '#bbdefb',
                    borderColor: '#2196f3',
                    lineTension: 0,
                    yAxisID: 'B',
                    data: dataY[0]
                }
            ]
        },
        options: {
            scales: {
                xAxes: [{
                    gridLines: {
                        color: "rgb(0,0,0,0)"
                    }
                }],
                yAxes: [{
                    gridLines: {color: "rgb(0,0,0,0)"},
                    id: 'A',
                    type: 'linear',
                    position: 'left',
                }, {
                    gridLines: {color: "rgb(0,0,0,0)"},
                    id: 'B',
                    type: 'linear',
                    position: 'right'
                }]
            },
            title: {
                display: true,
                text: 'حجم معاملات و قدرت خریدار',
                fontSize: 15,
                fontFamily: ['IRANSans', 'sans-serif'],
                fontColor: '#333'
            },
            legend: {
                labels: {
                    fontSize: 13,
                    fontFamily: ['IRANSans', 'sans-serif'],
                    fontColor: '#333'
                }
            },
            tooltips: tooltip()
        }
    })
}

function SaraneChart(dataX, dataY) {
    return new Chart($("#chartA"), {
        type: 'line',
        data: {
            labels: dataX,
            datasets: [
                {
                    label: "سرانه خرید",
                    fill: false,
                    backgroundColor: '#90caf9',
                    borderColor: '#2196f3',
                    lineTension: 0,
                    data: dataY[0]
                },
                {
                    label: "سرانه فروش",
                    fill: false,
                    backgroundColor: '#ef9a9a',
                    borderColor: '#f44336',
                    lineTension: 0,
                    data: dataY[1]
                }
            ]
        },
        options: {
            scales: {
                xAxes: [{
                    gridLines: {
                        color: "rgb(0,0,0,0)"
                    }
                }],
                yAxes: [{
                    gridLines: {
                        color: "rgb(0,0,0,0)"
                    }
                }]
            },
            title: {
                display: true,
                text: 'سرانه خرید/فروش',
                fontSize: 15,
                fontFamily: ['IRANSans', 'sans-serif'],
                fontColor: '#333'
            },
            legend: {
                labels: {
                    fontSize: 13,
                    fontFamily: ['IRANSans', 'sans-serif'],
                    fontColor: '#333'
                }
            },
            tooltips: tooltip()
        }
    });

}

function tooltip() {
    return {

        titleFontSize: 15,
        _titleFontFamily: ['IRANSans', 'sans-serif'],
        _bodyFontFamily: ['IRANSans', 'sans-serif'],
        bodyFontSize: 13
    }
}







