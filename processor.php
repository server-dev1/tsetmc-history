<?php

require __DIR__ . '/vendor/autoload.php';

use Carbon\Carbon;

$logger = setupLogger();
setDebugMode(false);

if (!isset($_GET['symbol'])) {
    jsonRes([
        "error" => "لطفا نام نماد را وارد کنید"
    ], 422);
}

if (isset($_GET['limit']) && is_numeric($_GET['limit'])) {
    $limit = $_GET['limit'];
}

// دریافت نماد و لینک
$symbol = getSymbol($_GET['symbol']);
$url = createUrl($symbol);
// دریافت اطلاعات به صورت جیسون
$response_json = sendRequest($url, function () {
    jsonRes([
        "error" => "خطا در اتصال به اینترنت"
    ], 400);
});
// تبدیل جیسون به آرایه
$res = json_decode($response_json, true);
// بررسی صحیح بودن نام نماد یا آیدی سهم
if (!is_array($res) || array_values($res)[0] == null) {
    jsonRes([
        "error" => "نماد وارد شده نامعتبر است لطفا نام نماد یا id نماد را وارد کنید"
    ], 422);
}

$history = [];

// تبدیل اطلاعات به آرایه های جداگانه
$tradeRows = explode(",", $res['0']);
$oldTrade = explode(";", $res['2']);
$oldClient = explode(";", $res['3']);

/*
last_price -> 3
first_price -> 1
close_price -> 2
low_price -> 8
high_price -> 9
number_tarde -> 4
volume_trade -> 5
value_trade -> 6
change_last_price -> 7
yesterday -> 10
*/

if (!empty($tradeRows) && !empty($tradeRows[0])) {
    // افزودن اطلاعات امروز
    $history[0] = [
        "today" => customFormat($tradeRows[45]) == Carbon::now()->format("Y-m-d"),
        "date" => customFormat($tradeRows[45]),
        "jDate" => getJDate(customFormat($tradeRows[45])),
        "first" => number_format($tradeRows[1]),
        "last" => number_format($tradeRows[3]),
        "close" => number_format($tradeRows[2]),
        "low" => number_format($tradeRows[8]),
        "high" => number_format($tradeRows[9]),
        "yasterday" => number_format($tradeRows[10]),
        "lastPercent" => number_format($tradeRows[7] / $tradeRows[3] * 100),
        "closePercent" => number_format(($tradeRows[2] - $tradeRows[10]) / $tradeRows[3] * 100),
        "tradeVolume" => $tradeRows[5],
        "tradeValue" => $tradeRows[6] / 10,
        "tradeVolume_view" => number_format($tradeRows[5]),
        "tradeValue_view" => number_format($tradeRows[6] / 1e7),
        "sarane_kh" => $tradeRows[40] / 10,
        "sarane_f" => $tradeRows[41] / 10,
    ];

    $history[0] = array_merge($history[0], [
        "sarane_kh_view" => number_format(round($history[0]['sarane_kh'], 1), 1),
        "sarane_f_view" => number_format(round($history[0]['sarane_f'], 1), 1),
        "buyer_ratio" => round($tradeRows[42], 2),
        "kharidVazni" => round($history[0]['sarane_kh'], 1) * $history[0]['tradeValue'],
        "foroshVazni" => round($history[0]['sarane_f'], 1) * $history[0]['tradeValue'],
    ]);
}

for ($i = 0, $iMax = count($oldTrade); $i < $iMax; $i++) {
    if (empty($oldTrade[$i]) || empty($oldClient[$i])) {
        continue;
    }

    $cols = explode(",", $oldTrade[$i]);
    $clientCols = explode(",", $oldClient[$i]);
    if ($i === 0) {
        $cols = array_slice($cols, 1);
        $clientCols = array_slice($clientCols, 1);
    }

    /*
     date -> 2
     first_price -> 1
     end_price -> 3
     last_pirce -> 4
     number_trade -> 5
     volume_trade -> 6
     value_trade -> 7
     change_end_price -> 8
     low_pirce -> 9
     hight_price -> 10
     yesterday_price ->11
    */

    $day = (int)$cols[0] + ($history[0]["today"] ? 0 : 1);
    $yesterdayPrice = $cols[11];
    // افزودن اطلاعات سابقه روز
    $lastPercent = (($cols[4] - $yesterdayPrice) / $yesterdayPrice) * 100;
    $history[$day] = [
        "date" => customFormat($cols[2]),
        "jDate" => getJDate(customFormat($cols[2])),
        "first" => number_format($cols[1]),
        "last" => number_format($cols[4]),
        "close" => number_format($cols[3]),
        "low" => number_format($cols[9]),
        "high" => number_format($cols[10]),
        "yasterday" => number_format($yesterdayPrice),
        "tradeVolume" => $cols[6],
        "tradeValue" => $cols[7] / 10,
        "closePercent" => (float)round($cols[8] / $yesterdayPrice * 100, 2),
        "lastPercent" => number_format($lastPercent, 2)
    ];

    $history[$day] = array_merge($history[$day], [
        "tradeVolume_view" => number_format($history[$day]['tradeVolume']),
        "tradeValue_view" => number_format($history[$day]['tradeValue'] / 1e6),
    ]);


    // افزودن اطلاعات حقیقی - حقوقی
    $history[$day] = array_merge($history[$day], [
        "Buy_CountI" => $clientCols[1],
        "Buy_I_Volume" => $clientCols[3],
        "Sell_CountI" => $clientCols[5],
        "Sell_I_Volume" => $clientCols[7]
    ]);

    // افزودن سرانه ها
    if ($history[$day]["Buy_CountI"] == 0) {
        $sarane_kh = 0;
        $sarane_kh_view = 0;
    } else {
        $sarane_kh = $history[$day]["Buy_I_Volume"] / $history[$day]["Buy_CountI"] * $cols[3];
        $sarane_kh_view = number_format(round($sarane_kh / 1e7, 1), 1);
    }

    if ($history[$day]["Sell_CountI"] == 0) {
        $sarane_f = 0;
        $sarane_f_view = 0;
    } else {
        $sarane_f = $history[$day]["Sell_I_Volume"] / $history[$day]["Sell_CountI"] * $cols[3];
        $sarane_f_view = number_format(round($sarane_f / 1e7, 1), 1);
    }

    $history[$day] = array_merge($history[$day], [
        "sarane_kh" => $sarane_kh / 10,
        "sarane_f" => $sarane_f / 10,
        "sarane_kh_view" => $sarane_kh_view,
        "sarane_f_view" => $sarane_f_view,
        "kharidVazni" => $sarane_kh_view * $history[$day]['tradeValue'],
        "foroshVazni" => $sarane_f_view * $history[$day]['tradeValue'],
    ]);

    // افزوودن قدرت خریدار
    if ($sarane_f == 0 || $sarane_kh == 0) {
        $buyer_ratio = 0;
    } else {
        $buyer_ratio = round($sarane_kh / $sarane_f, 2);
    }

    $history[$day] = array_merge($history[$day], [
        "buyer_ratio" => $buyer_ratio,
    ]);
}

// مرتب سازی بر اساس تاریخ
usort($history, static function ($a, $b) {
    return strtotime($b["date"]) - strtotime($a["date"]);
});

// برش آرایه تاریخچه به اندازه خواسته شده
$history = array_slice($history, 0, isset($limit) ? $limit : count($history));

$response = compact('history');

// اضافه کردن سرانه خرید و فروش و نسبت وزنی
foreach ($history as $item) {
    $response["SumTradeValue"] += $item['tradeValue'];
    $response["SumKharidVazni"] += $item['kharidVazni'];
    $response["SumForoshVazni"] += $item['foroshVazni'];
}

$response["AvgKharidVazni"] = round($response["SumKharidVazni"] / $response["SumTradeValue"], 2);
$response["AvgForoshVazni"] = round($response["SumForoshVazni"] / $response["SumTradeValue"], 2);
$response["GhodratVazni"] = round($response["AvgKharidVazni"] / $response["AvgForoshVazni"], 2);

$response["symbolId"] = $symbol;
$response["symbolName"] = $_GET["symbol"];
// ارسال اطلاعات به صورات جیسون
jsonRes($response);
